// dropdowns.js
import { log, DROPDOWN_OPTIONS } from './config.js';

const cachedElements = new Map();

export function getElement(id) {
  if (!cachedElements.has(id)) {
    const element = document.getElementById(id);
    log('elements', `Getting element: ${id}`, { found: !!element });
    cachedElements.set(id, element);
  }
  return cachedElements.get(id);
}

export function populateDropdowns() {
  log('init', 'Populating dropdowns');
  Object.entries(DROPDOWN_OPTIONS).forEach(([dropdownId, options]) => {
    const select = getElement(dropdownId);
    if (select) {
      select.innerHTML = '';
      Object.entries(options).forEach(([value, text]) => {
        const option = document.createElement('option');
        option.value = value;
        option.textContent = text;
        select.appendChild(option);
      });
      select.value = Object.keys(options)[0];
    }
  });
}

export function getInputIds() {
  return ['reistestrecke', ...Object.keys(DROPDOWN_OPTIONS)];
}
