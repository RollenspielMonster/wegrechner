// main.js
import { log } from './config.js';
import { populateDropdowns, getElement, getInputIds } from './dropdowns.js';
import { generateTravelModesHTML, initializeTravelModesValues, calculate } from './travel.js';
import { ThemeManager } from './theme.js';

function initializeEventListeners() {
  const inputs = getInputIds();
  inputs.forEach(id => {
    const element = getElement(id);
    if (element) {
      element.addEventListener('input', () => calculate());
      element.addEventListener('change', () => calculate());
    }
  });
}

function initialize() {
  log('init', 'Starting initialization');
  generateTravelModesHTML();
  initializeTravelModesValues();
  populateDropdowns();
  initializeEventListeners();
  new ThemeManager();
  calculate();
  log('init', 'Initialization complete');
}

window.onload = initialize;
window.calculate = calculate;
