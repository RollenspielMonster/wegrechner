// travel.js
import { TRAVEL_SPEEDS, MODIFIERS, log } from './config.js';
import { getElement } from './dropdowns.js';

export function checkSpecialConditions(transportType, conditions) {
  const transport = TRAVEL_SPEEDS[transportType];
  if (!transport) return true;

  if (transport.terrain_blocked && 
      transport.terrain_blocked.includes(conditions.surface)) {
    return false;
  }

  return true;
}

export function calculateModifier(transportType, conditions) {
  const transport = TRAVEL_SPEEDS[transportType];
  let modifier = 1.0;

  Object.entries(conditions).forEach(([condition, value]) => {
    if (MODIFIERS[condition] && MODIFIERS[condition][value]) {
      modifier *= MODIFIERS[condition][value];
    }
  });

  return modifier;
}

export function calculateTravelDays(distance, speedRange, conditions) {
  if (!distance || distance <= 0) return 0;

  const modifier = calculateModifier(speedRange.type, conditions);
  const averageSpeed = (speedRange.min + speedRange.max) / 2;
  return Math.max(1, Math.round(distance / (averageSpeed * modifier)));
}

// Update UI with error message if present
function updateUI(error = null) {
  const errorDisplay = getElement('errorMessage');
  if (!errorDisplay) return;

  if (error) {
    errorDisplay.textContent = error.message || 'Ein Fehler ist aufgetreten.';
    errorDisplay.classList.remove('d-none');
  } else {
    errorDisplay.textContent = '';
    errorDisplay.classList.add('d-none');
  }
}

export function generateTravelModesHTML() {
  log('init', 'Generating travel modes HTML');
  const container = document.getElementById('travel-modes-container');
  if (!container) {
    log('errors', 'Travel modes container not found');
    return;
  }

  const html = Object.entries(TRAVEL_SPEEDS)
    .map(([key, data]) => `
      <div class="row mb-2 align-items-center" data-travel-mode="${key}">
        <div class="col-4">${data.label}</div>
        <div class="col-4 disabledinput text-center">
          <input type="text" id="${key}_min" disabled /> - 
          <input type="text" id="${key}_max" disabled />
        </div>
        <div class="col-4">
          <input type="text" class="days" id="${key}_day" readonly />
        </div>
      </div>
    `).join('');

  container.innerHTML = html;
  log('init', 'Travel modes HTML generated');
}

// Initialize travel mode values
export function initializeTravelModesValues() {
  log('init', 'Initializing travel mode values');
  Object.entries(TRAVEL_SPEEDS).forEach(([key, data]) => {
    const minElement = document.getElementById(`${key}_min`);
    const maxElement = document.getElementById(`${key}_max`);

    if (minElement) minElement.value = data.min;
    if (maxElement) maxElement.value = data.max;
  });
  log('init', 'Travel mode values initialized');
}

export function calculate() {
  try {
    log('calc', 'Starting calculation');
    const distance = getElement('reistestrecke').value;

    const conditions = {
      surface: getElement('surface').value,
      weather: getElement('weather').value,
      season: getElement('season').value,
      health: getElement('health').value,
      group_type: getElement('group_type').value,
      load: getElement('load').value,
      daylight: getElement('daylight')?.value || 'full',
      visibility: getElement('visibility')?.value || 'clear',
      road_condition: getElement('road_condition')?.value || 'good',
      water_conditions: getElement('water_conditions')?.value || 'calm'
    };

    Object.entries(TRAVEL_SPEEDS).forEach(([key, speeds]) => {
      const dayElement = document.getElementById(`${key}_day`);
      if (dayElement) {
        if (checkSpecialConditions(key, conditions)) {
          const days = calculateTravelDays(distance, {...speeds, type: key}, conditions);
          dayElement.value = days || '0';
        } else {
          dayElement.value = 'N/A';
        }
      }
    });

    updateUI();
    log('calc', 'Calculation completed');
  } catch (error) {
    log('errors', 'Calculation error:', error);
    updateUI(error);
  }
}
