// theme.js
import { getElement } from './dropdowns.js';

export class ThemeManager {
  constructor() {
    this.html = document.documentElement;
    this.themeToggle = getElement('themeToggle');
    this.themeIcon = this.themeToggle?.querySelector('.theme-icon');
    this.init();
  }

  init() {
    this.setInitialTheme();
    this.bindEvents();
  }

  setInitialTheme() {
    const prefersDark = window.matchMedia('(prefers-color-scheme: dark)').matches;
    this.setTheme(prefersDark ? 'dark' : 'light');
  }

  setTheme(theme) {
    this.html.setAttribute('data-bs-theme', theme);
    if (this.themeIcon) {
      this.themeIcon.textContent = theme === 'dark' ? '🌙' : '☀️';
    }
  }

  bindEvents() {
    if (this.themeToggle) {
      this.themeToggle.addEventListener('click', () => {
        const currentTheme = this.html.getAttribute('data-bs-theme');
        this.setTheme(currentTheme === 'dark' ? 'light' : 'dark');
      });
    }

    window.matchMedia('(prefers-color-scheme: dark)')
      .addEventListener('change', e => this.setTheme(e.matches ? 'dark' : 'light'));
  }
}
